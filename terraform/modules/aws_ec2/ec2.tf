# aws ec2 describe-images \
#     --owners amazon \
#     --filters "Name=name,Values=al2023-ami-2023*" \
#               "Name=virtualization-type,Values=hvm" \
#               "Name=architecture,Values=x86_64" \
#               "Name=root-device-type,Values=ebs" \
#               "Name=is-public,Values=true" \
#     --query 'Images[*].[ImageId,Name,CreationDate]' \
#     --output table

data "aws_ami" "amzn" {
  most_recent = true

  filter {
    name   = "name"
    values = ["al2023-ami-2023*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  owners = ["amazon"]
}

resource "aws_instance" "ec2" {
  count                  = var.instances_count
  ami                    = data.aws_ami.amzn.id
  instance_type          = "t2.micro"
  key_name               = aws_key_pair.ssh_key.key_name
  availability_zone      = var.az
  subnet_id              = var.aws_subnet_id
  vpc_security_group_ids = [var.aws_security_group_id]

  tags = var.tags
}